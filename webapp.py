#!/usr/bin/env python
# -*- coding: utf-8 -*-
import subprocess
from subprocess import check_output
from flask import Flask, render_template, Response ,request, redirect , session, url_for
from flask_script import Manager
import datetime
from camera_pi import Camera
import io
import json
import urllib
import urllib2
import requests
import jwt
from firebase import firebase

app = Flask(__name__,template_folder='templates')
app.secret_key = '2xGaLqp'
firebase = firebase.FirebaseApplication('https://apartment-fa38c.firebaseio.com/', None)

def get_resource_as_string(name, charset='utf-8'):
	with app.open_resource(name) as f:
		return f.read().decode(charset)
app.jinja_env.globals['get_resource_as_string'] = get_resource_as_string

def get_data():
	"""
	Return a string that is the output from subprocess
	"""

	# There is a link above on how to do this, but here's my attempt
	# I think this will work for your Python 2.6

	p = subprocess.Popen(["tree", "/Desktop/testcam/qrcode.py"], stdout=subprocess.PIPE)
	out, err = p.communicate()

	return out

def readfirebase(name):
	result = firebase.get(name, 'amount')
	return result
def updatefirebase(name,money):
	result = firebase.put(name,'amount', money)
	return result
def newdatafirebase(name,data):
	temp = {"amount":data}
	result = firebase.post(name,temp)
@app.route("/",methods=['GET', 'POST'])
def get():
	now = datetime.datetime.now()
	timeString = now.strftime("%Y-%m-%d %H:%M")
	if(request.method == 'GET'):
		if 'Apartment_id' in session:
			url = 'http://seniorproject-1234.appspot.com/raspi/login'
			url += "?id="+session['Apartment_id']+"&action=session"
			r = requests.get(url).json()
			Apartment_name = r['Apartment'][0]['name']
			Apartment_tel = r['Apartment'][0]['telephone']
			data_service = r['Service']
			act = request.args.get('action')
			if (act == "cal"):
				action = "cal"
			else:
				action = "home"
			templateData = {'name':Apartment_name,'tel':Apartment_tel,'ser':data_service,'action':action}
			   #print json.dumps(data_apartment)
			return render_template('main.html', **templateData)
		else:
			return render_template('login.html')
	else:
		if 'Apartment_id' in session:
			url = 'http://seniorproject-1234.appspot.com/raspi/login'
			url += "?id="+session['Apartment_id']+"&action=session"
			r = requests.get(url).json()
			Apartment_name = r['Apartment'][0]['name']
			Apartment_tel = r['Apartment'][0]['telephone']
			data_service = r['Service']
			act = request.form('action')
			if (act == "cal"):
				action = "cal"

			else:
				action = "home"
			templateData = {'name':Apartment_name,'tel':Apartment_tel,'ser':data_service,'action':action}
			   #print json.dumps(data_apartment)
			return render_template('main.html', **templateData)
		else:
			return render_template('login.html')


@app.route('/login',methods=['POST'])
def sendlogin():
	try:
		if(request.method == 'POST'):
			url = 'http://seniorproject-1234.appspot.com/raspi/login'
			username = request.form['username']
			password = request.form['password']
			url += "?id="+username+"&password="+password+"&action=login"
			print url
			r = requests.get(url).json()
			Apartment_name = r['Apartment'][0]['name']
			Apartment_tel = r['Apartment'][0]['telephone']
			Apartment_id = r['Apartment'][0]['id']
			data_service = r['Service']
			session['username'] = username
			session['Apartment_id'] = Apartment_id
			templateData = {'name':Apartment_name,'tel':Apartment_tel,'ser':data_service}
			#print json.dumps(data_apartment)
			return render_template('main.html', **templateData)
		else:
			return redirect("/")
	except Exception as e:
		return redirect("/")

@app.route('/payment',methods=['POST'])
def payment():
	price = request.form['price']
	ser_id = request.form['serid']
	ser_type = request.form['type']
	ser_name = request.form['name']
	templateData = {'price':price,'serid':ser_id,'name':ser_name}
	if(int(ser_type) == 3):
		url = ''
		data = {'action': 'cal'}
		return redirect(url_for('cal'))
	else:
		return render_template('payment.html', **templateData)

@app.route('/calculator',methods=['GET'])
def cal():
	if 'Apartment_id' in session:
		url = 'http://seniorproject-1234.appspot.com/raspi/login'
		url += "?id="+session['Apartment_id']+"&action=session"
		r = requests.get(url).json()
		Apartment_name = r['Apartment'][0]['name']
		Apartment_tel = r['Apartment'][0]['telephone']
		data_service = r['Service']
		ser_id = request.args.get('serid')
		ser_name = request.args.get('name')
		templateData = {'serid':ser_id,'nameser':ser_name,'name':Apartment_name,'tel':Apartment_tel,'ser':data_service}
		return render_template('calculator.html', **templateData)

@app.route('/logout',methods=['GET'])
def logout():
	session.clear()
	return redirect("/")

@app.route('/video')
def video():
	try:
		out = check_output(["sudo", "python","qrcode.py"])
		#out = check_output(["pwd"])
		return str(out)
	except:
		return str("Error")


@app.route('/checkuser',methods=['GET'])
def checkuser():
	url = 'http://seniorproject-1234.appspot.com/raspi/checkuser'
	encoded = request.args.get('code')

	try:
		data = {'qrcode':encoded}
		r = requests.post(url,data=data).json()
		return json.dumps(r)
	except:
		return ("-1")

@app.route('/submitpayment',methods=['POST'])
def subpay():
	url = 'http://seniorproject-1234.appspot.com/raspi/payment'
	userid = request.form['cusid']
	serid = request.form['serid']
	price = request.form['price']
	amount = readfirebase(userid)
	data = {'cusid': userid, 'serid':serid, 'price':price}
	r = requests.post(url,data=data)
	if(float(price) > 0 and (float(price) <= float(amount))):
		balance = float(amount)-float(price)
		if(r.content == "Payment Finish" ):
			updatefirebase(userid,balance)
	return r.content

@app.route('/testfirebase',methods=['GET'])
def testfb():
	return updatefirebase('231120161479892693',253950)


if __name__ == "__main__":
	app.run(host='0.0.0.0', port=80, debug=True)
